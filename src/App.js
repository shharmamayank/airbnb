import CardFlex from "./components/CardFlex/CardFlex";
import FilterByCategories from "./components/Filter/FilterByCategories";
import NavBar from "./components/Navbar/NavBar";
import "./App.css"
import Footer from "./components/Footer/Footer";
import { Routes, Route } from "react-router-dom"
import ProductsPerPage from "./components/ProductsPerPage/ProductsPerPage";
import FooterPerPage from "./components/FooterPerPage/FooterPerPage";
import CheckOutPage from "./components/CheckOutPage/CheckOutPage";
import AmazingFilter from "./components/AmazingFilter/AmazingFilter";
import LakeView from "./components/Lakeview/LakeView";
import Farm from "./components/Farm/Farm";
function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={
          <>
            <NavBar />
            <FilterByCategories />
            <div className="cards">
              <CardFlex />
            </div>
            <Footer />
          </>
        } />
        <Route path="ProductPerPage/:Cards" element={<><ProductsPerPage /> <FooterPerPage /></>} />
        <Route path="ProductPerPage/:Cards/checkout" element={<CheckOutPage />} />
        <Route path="Amazing" element={<><NavBar /> <FilterByCategories /><AmazingFilter /> </>} />
        <Route path="Top of the world" element={<><NavBar /> <FilterByCategories /><LakeView /> </>} />
        <Route path="Farms" element={<><NavBar /> <FilterByCategories /><Farm /> </>} />
        <Route path="Amazing/ProductPerPage/:Cards" element={<><ProductsPerPage /> <FooterPerPage /></>} />
        <Route path="Farms/ProductPerPage/:Cards" element={<><ProductsPerPage /> <FooterPerPage /></>} />
        <Route path="Top of the world/ProductPerPage/:Cards" element={<><ProductsPerPage /> <FooterPerPage /></>} />
        
      </Routes>

    </>
  );
}

export default App;
