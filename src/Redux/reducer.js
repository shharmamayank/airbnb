import { createSlice } from '@reduxjs/toolkit'
import { links } from '../assests/images-links.js'
import { list } from '../assests/Card-list.js'
import {roomsLinks } from '../assests/rooms-details.js'
console.log("sajk")
const initialState = {
    data: links,
    cardData: list,
    counter: 0,
    rooms:roomsLinks 
}

const imageLinkSlice = createSlice({
    name: "categoreyBar",
    initialState,
    reducers: {
        increaseFive: (state, { payload }) => {
            state.counter = state.counter + payload
            console.log(state.counter);
        }
    }
})
// export const { increaseFive } = imageLinkSlice.actions
export default imageLinkSlice.reducer