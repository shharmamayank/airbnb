import { configureStore } from '@reduxjs/toolkit'
import categoryReducer from './reducer'

export const store =configureStore({
    reducer:{
        categoreyBar: categoryReducer
    }
})