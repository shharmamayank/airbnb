import React from 'react'
import { links } from '../../assests/images-links.js'
import { Link } from 'react-router-dom';
import "./FilterByCategories.css"
import { } from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux'
import { increaseFive } from '../../Redux/reducer.js'
function FilterByCategories() {
    // categoreyBar: categoryReducer
    const { data } = useSelector(store => store.categoreyBar)
    // const dispatch=useDispatch()
    // console.log(data);
    // const alldata =
    // console.log(alldata)
    {/* {alldata} */ }
    return (
        <>
            <div className='filter-div'>
                {data.map((data, i) => (
                    <div className="link-image-container">
                        <Link className='text-decoration-none' to={`${data.label}`}>
                            <img key={i} src={data.imgSrc} className='links-img' />
                            <p className='links-label'>{data.label}</p>
                        </Link>
                    </div>
                ))}
            </div>
           


        </>
    )

}

export default FilterByCategories
