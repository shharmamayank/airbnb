import React from 'react'
import "./CardFlex.css"
import hotels from '../../assests/hotels/hotel-1.jpeg'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
function CardFlex() {
    const { cardData } = useSelector(store => store.categoreyBar)
    return (
        <>
            <Link to={"ProductPerPage/:Cards"} className='text-decoration-none' >
                <div className='d-flex flex-wrap justify-content-center ' id="card-box-image">
                    {cardData.map((data, index) => {
                        return (
                            <>
                                <div className='text-and-card rounded ms-2'>
                                    <div id={`carouselExampleIndicators${index}`} class="carousel slide d-flex  ">
                                        <div class="carousel-indicators ">
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                        </div>
                                        <div class="d-flex carousel-inner rounded-4">
                                            <div class="carousel-item active">
                                                <img src={data.imgSrc[0]} class="d-block  " id="homepage-card-image" alt="..." />
                                            </div>
                                            <div class="carousel-item">
                                                <img src={data.imgSrc[1]} class="d-block " id="homepage-card-image" alt="..." />
                                            </div>
                                            <div class="carousel-item">
                                                <img src={data.imgSrc[2]} class="d-block " id="homepage-card-image" alt="..." />
                                            </div>
                                        </div>
                                        <button class="carousel-control-prev" type="button" data-bs-target={`#carouselExampleIndicators${index}`} data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target={`#carouselExampleIndicators${index}`} data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                    <div className="card-info-flex">
                                        <div className="card-title">
                                            <p><b>{data.title}</b></p>
                                            <p>{data.date}</p>
                                            <p><span><b>₹{data.price}</b></span> night</p>
                                        </div>
                                        <div className="card-rating">
                                            <p><i class="fa-solid fa-star"></i>{data.rating}</p>
                                        </div>
                                    </div>
                                </div>

                            </>
                        )
                    })}

                </div>
            </Link>

        </>
    );
}
export default CardFlex
