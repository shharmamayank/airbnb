import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css'
import logo from "../../assests/long-logo.png";
import flexible from "../../assests/maps/Flexible.jpg"
import middleEast from "../../assests/maps/middleEast.webp"
import europe from "../../assests/maps/europe.webp"
import canada from "../../assests/maps/canada.webp"
import unitedKingdom from "../../assests/maps/unitedKingdom.webp"
import thailand from "../../assests/maps/thailand.webp"
import "./NavBar.css"
import Login from '../LoginAndSignUp/Login';

function NavBar() {
    const [check, setCheck] = useState(false);
    const [onChangeValue, setOnChangeValue] = useState(new Date());
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [AdultCounter, SetAdultCounter] = useState(0);
    const [InfantCounter, SetInfantCounter] = useState(0);
    const [PetCounter, SetPetCounter] = useState(0);
    const [ChildCounter, SetChildCounter] = useState(0);

    const handOnSelect = (ranges) => {
        setStartDate(ranges.selection.startDate);
        setEndDate(ranges.selection.endDate)
    }

    const selectionRange = {
        startDate: startDate,
        endDate: endDate,
        key: "selection"
    }
    return (
        <>
            <div className='navbar'>
                <Link to="/"> <img src={logo} alt="" className='navbar-img' /></Link> 
                <div className='search-bar'>
                    <button class="search-bar-text" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasTop" aria-controls="offcanvasTop">Any Where</button>
                    <div class="offcanvas offcanvas-top " tabindex="5" id="offcanvasTop" aria-labelledby="offcanvasTopLabel">
                        <div class="offcanvas-body">
                            <div className='navbar'>
                                <img src={logo} alt="" className='navbar-img' />
                                <div className='search-bar'>
                                    <div className='search-bar-text'>Stays</div>
                                    <div className='search-bar-text'>Experience</div>
                                    <div className='search-bar-text3'>Online Experience</div>
                                    <div className='search-bar-icon-div'><i class="fa-solid fa-magnifying-glass"></i></div>
                                </div>
                                <div className='profile-container'>
                                    <div className='airbnb-your-home'>Airbnb your home</div>
                                    <div className='airbnb-your-home'> <i class="fa-sharp fa-solid fa-globe"></i></div>
                                    <div class="btn-group dropstart">
                                        <button class="btn btn-secondary text-secondary bg-light rounded-pill" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="fa-solid fa-bars"></i>
                                            <i class="fa-solid fa-circle-user"></i>
                                        </button>
                                        <ul class="dropdown-menu ">
                                            <li> <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                <a class="dropdown-item" href="#"><b>Sign Up</b></a></button></li>
                                            <li><a class="dropdown-item" href="#">Login</a></li>
                                            <div style={{ height: "1px", backgroundColor: "var(--grey)", width: "100%" }}></div>
                                            <li><a class="dropdown-item" href="#">Airbnb your home</a></li>
                                            <li><a class="dropdown-item" href="#">Help</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className='second-navbar-main-container d-flex justify-content-center'>
                                <div className='navbar2-second-bar'>
                                    <div className='d-flex-check'>
                                        <div className='map-conatiner'>
                                            <div class="dropdown where-input-active ">
                                                <button className="btn where-input-container" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <label style={{ marginTop: "1rem" }}>where</label>
                                                    <input className='input-container-where-box' style={{ border: "none", }} placeholder='Search Destination' />
                                                </button>
                                                <div class="w-100 dropdown-menu">
                                                    <div className='dropdown-menu-for-images d-flex flex-column bg-light rounded' >
                                                        <div className='image-searchbar-map-container-1'>
                                                            <div className=' image-container-box d-flex flex-column rounded '><img className='image-searchbar-map' src={flexible} alt="" />Flexible</div>
                                                            <Link className='text-decoration-none' to="Amazing"> <div className=' image-container-box d-flex flex-column rounded'><img className='image-searchbar-map' src={europe} alt="" />Europe</div></Link>
                                                            <Link className='text-decoration-none' to="Farms">  <div className=' image-container-box d-flex flex-column rounded'><img className='image-searchbar-map' src={canada} alt="" />canada</div></Link>
                                                        </div>
                                                        <div className='image-searchbar-map-container-1'>
                                                            <div className=' image-container-box d-flex flex-column rounded'><img className='image-searchbar-map' src={thailand} alt="" />thailand</div>
                                                            <div className=' image-container-box d-flex flex-column rounded'><img className='image-searchbar-map' src={unitedKingdom} alt="" />unitedKingdom</div>
                                                            <div className=' image-container-box d-flex flex-column rounded'><img className='image-searchbar-map' src={middleEast} alt="" />middleEast</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='second-navbar-checkin'>
                                            <div class="dropdown d-flex flex-column">
                                                <button class="btn checkin-container-navbar2" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <span style={{ paddingTop: "1rem" }}>check in</span>
                                                    <span>Add Dates</span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-center">
                                                    <li>
                                                        {/* <div>
                                                                <DateRangePicker showDoubleView ranges={[selectionRange]} minDate={new Date()} rangeColors={["black"]} onChange={handOnSelect} />
                                                            </div> */}
                                                        <Calendar showDoubleView />

                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div className='second-navbar-checkout'>
                                            <div class="dropdown">
                                                <button class="btn checkin-container-navbar2" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <span style={{ paddingTop: "1rem" }}>check out</span>
                                                    <span>Add Dates</span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-center">
                                                    <li>
                                                        {/* <div>
                                                                <DateRangePicker ranges={[selectionRange]} minDate={new Date()} rangeColors={["black"]} onChange={handOnSelect} />
                                                            </div> */}
                                                        <Calendar showDoubleView />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className='guest-who'>
                                            <div class="dropdown ">
                                                <button class="btn checkin-container-navbar2" type="button" data-bs-toggle="dropdown" aria-expanded="true">
                                                    <span style={{ paddingTop: "1rem", paddingBottom: "0" }}>Who</span>
                                                    <span>Add guest</span>
                                                </button>
                                                <div class="dropdown-menu guest-who-quantity"  >
                                                    <div className='adult-container d-flex justify-content-between'>
                                                        <div style={{ margin: "1rem" }}>
                                                            <span style={{ margin: "1rem" }}>Adult</span>
                                                        </div>
                                                        <div className='counter-qunatity' style={{ margin: "1rem" }}>
                                                            <button className="btn-for-increase" onClick={() => { SetAdultCounter(AdultCounter + 1) }}>+</button>
                                                            <h3>{AdultCounter}</h3>
                                                            <button className="btn-for-increase" onClick={() => { SetAdultCounter(AdultCounter - 1) }}>- </button>

                                                        </div>

                                                    </div>
                                                    <hr></hr>
                                                    <div className='adult-container d-flex justify-content-between'>
                                                        <div style={{ margin: "1rem" }}>
                                                            <span style={{ margin: "1rem" }}>Children</span>
                                                        </div>
                                                        <div className='counter-qunatity' style={{ margin: "1rem" }}>

                                                            <button className="btn-for-increase" onClick={() => { SetChildCounter(ChildCounter + 1) }}>+</button>
                                                            <h3>{ChildCounter}</h3>
                                                            <button className="btn-for-increase" onClick={() => { SetChildCounter(ChildCounter - 1) }}>-</button>
                                                        </div>

                                                    </div>
                                                    <hr></hr>
                                                    <div className='adult-container d-flex justify-content-between'>
                                                        <div style={{ margin: "1rem" }}>
                                                            <span style={{ margin: "1rem" }}>Infants</span>
                                                        </div>
                                                        <div className='counter-qunatity' style={{ margin: "1rem" }} >

                                                            <button className="btn-for-increase" onClick={() => { SetInfantCounter(InfantCounter + 1) }}>+</button>
                                                            <h3>{InfantCounter}</h3>
                                                            <button className="btn-for-increase" onClick={() => { SetInfantCounter(InfantCounter - 1) }}>-</button>
                                                        </div>

                                                    </div>
                                                    <hr></hr>
                                                    <div className='adult-container d-flex justify-content-between'>
                                                        <div style={{ margin: "1rem" }}>
                                                            <span style={{ margin: "1rem" }}>Pet</span>
                                                        </div>
                                                        <div className='counter-qunatity' style={{ margin: "1rem" }} >

                                                            <button className="btn-for-increase" onClick={() => { SetPetCounter(PetCounter + 1) }}>+</button>
                                                            <h3>{PetCounter}</h3>
                                                            <button className="btn-for-increase" onClick={() => { SetPetCounter(PetCounter - 1) }}>-</button>
                                                        </div>

                                                    </div>
                                                    <hr></hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="search-bar-text" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasTop" aria-controls="offcanvasTop">Any Week</button>
                    <div class="offcanvas offcanvas-top " tabindex="5" id="offcanvasTop" aria-labelledby="offcanvasTopLabel">
                        <div className='navbar'>
                            <img src={logo} alt="" className='navbar-img' />
                            <div className='search-bar'>
                                <div className='search-bar-text'>Stays</div>
                                <div className='search-bar-text'>Experience</div>
                                <div className='search-bar-text3'>Online Experience</div>
                                <div className='search-bar-icon-div'><i class="fa-solid fa-magnifying-glass"></i></div>
                            </div>
                            <div className='profile-container'>
                                <div className='airbnb-your-home'>Airbnb your home</div>
                                <div className='airbnb-your-home'> <i class="fa-sharp fa-solid fa-globe"></i></div>
                                <div class="btn-group dropstart">
                                    <button class="btn btn-secondary text-secondary bg-light rounded-pill" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fa-solid fa-bars"></i>
                                        <i class="fa-solid fa-circle-user"></i>
                                    </button>
                                    <ul class="dropdown-menu ">
                                        <li> <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <a class="dropdown-item" href="#"><b>Sign Up</b></a></button></li>
                                        <li><a class="dropdown-item" href="#">Login</a></li>
                                        <div style={{ height: "1px", backgroundColor: "var(--grey)", width: "100%" }}></div>
                                        <li><a class="dropdown-item" href="#">Airbnb your home</a></li>
                                        <li><a class="dropdown-item" href="#">Help</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="search-bar-text" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasTop" aria-controls="offcanvasTop">Any Guest</button>
                    <div class="offcanvas offcanvas-top " tabindex="5" id="offcanvasTop" aria-labelledby="offcanvasTopLabel">
                    </div>
                    <div className='search-bar-icon-div'><i class="fa-solid fa-magnifying-glass"></i></div>
                </div>
                <div className='profile-container'>
                    <div className='airbnb-your-home'>Airbnb your home</div>
                    <div className='airbnb-your-home'> <i class="fa-sharp fa-solid fa-globe"></i></div>
                    <div class="btn-group dropstart">
                        <button class="btn btn-secondary text-secondary bg-light rounded-pill" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa-solid fa-bars"></i>
                            <i class="fa-solid fa-circle-user"></i>
                        </button>
                        <ul class="dropdown-menu ">
                            <li>
                                <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <a class="dropdown-item" href="#"><b>Sign Up</b></a>
                                </button>
                            </li>
                            <li><a class="dropdown-item" href="#">Login</a></li>
                            <div style={{ height: "1px", backgroundColor: "var(--grey)", width: "100%" }}></div>
                            <li><a class="dropdown-item" href="#">Airbnb your home</a></li>
                            <li><a class="dropdown-item" href="#">Help</a></li>
                        </ul>
                    </div>
                </div>
                {/* {check && <div>
                    <DateRangePicker ranges={[selectionRange]} />
                </div>} */}
            </div>
            <Login />
        </>
    )
}

export default NavBar
