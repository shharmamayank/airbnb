import React from 'react'
import logo from "../../assests/long-logo.png";
import "./CheckOut.css"
import { Link } from 'react-router-dom';
function CheckOutPage() {
    return (
        <>
            <div>
                <Link to="/" > <img src={logo} alt="" className='checkout-navbar-img' /></Link>
                <hr></hr>
            </div>
            <div>
                <h3>Request to book</h3>
                <h4>Your trip</h4>
                <div>
                    <div className='guests-and-number-container'>
                        <div>
                            <h4>Dates</h4>
                            <p> 14–19 May</p>
                        </div>
                        <div>
                            Edit
                        </div>
                    </div>
                    <div className='guests-and-number-container'>
                        <div>
                            <h4>Guests</h4>
                            <p>  1 guest </p>
                        </div>
                        <div>
                            Edit
                        </div>
                    </div>
                    <hr></hr>
                </div>
                <div>
                    <div className="paynow-container">
                        <div>
                            <h3>Pay now</h3>
                        </div>
                        <div>
                            <img src="//a0.muscache.com/airbnb/static/packages/assets/frontend/legacy-shared/svgs/payments/logo_mastercard.f18379cf1f27d22abd9e9cf44085d149.svg" alt="Mastercard" height="9" aria-hidden="true"></img>
                            <img src="//a0.muscache.com/airbnb/static/packages/assets/frontend/legacy-shared/svgs/payments/logo_visa.0adea522bb26bd90821a8fade4911913.svg" alt="Visa Card" height="9" aria-hidden="true"></img>
                            <img src="//a0.muscache.com/airbnb/static/packages/assets/frontend/legacy-shared/svgs/payments/logo_rupay.f419bf8f3062eb6d2408393354129ba8.svg" height="9" aria-hidden="true"></img>
                            <img src="//a0.muscache.com/airbnb/static/packages/assets/frontend/legacy-shared/svgs/payments/logo_upi.1762e17397c0d6e85fb10050020d93f4.svg" alt="upi" height="9" aria-hidden="true"></img>
                        </div>
                    </div>
                </div>
                <div className='drop-of-cards'>
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Credit/ Debit Card
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Pay UPI </a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </>
    )
}

export default CheckOutPage
