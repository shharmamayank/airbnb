import React from 'react'

function FooterPerPage() {
    return (
        <>
            <div className='footer-per-page-main-container bg-secondary'>
                <div className='footer-per-page-header'>
                    <h1>
                        Explore other options in and around Tandi
                    </h1>
                </div>
                <div className='footer-other-places d-flex justify-content-around'>
                    <div>
                        <p>Manali</p>
                        <p>Dharamshala</p>
                        <p>Sahibzada Ajit Singh Nagar</p>

                    </div>
                    <div>
                        <p>Shimla</p>
                        <p>Kasol</p>
                        <p>Palampur</p>

                    </div>
                    <div>
                        <p>Bir</p>
                        <p>Jibhi</p>
                        <p>New Delhi</p>
                    </div>
                    <div>
                        <p>Bir</p>
                        <p>Jibhi</p>
                        <p>New Delhi</p>
                    </div>

                </div>
                <div>
                    <p>Airbnb India </p>
                </div>
                <div className='supoort-and-community d-flex justify-content-evenly'>
                    <div className='support'>
                        <p>Help Care</p>
                        <p>AirCover</p>
                        <p>Supporting people with disabilities</p>
                        <p>Cancellation options</p>
                        <p>Our COVID-19 Response</p>
                        <p>Report a neighbourhood concern</p>
                    </div>
                    <div className='Community'>
                        <h4>Community</h4>
                        <p>Airbnb.org: disaster relief housing</p>
                        <p>Combating discrimination</p>
                    </div>
                    <div className='Hosting'>
                        <h4>Hosting</h4>
                        <p>Airbnb your home</p>
                        <p>AirCover for Hosts</p>
                        <p>Explore hosting resources</p>
                        <p>Visit our community forum</p>
                        <p>How to host responsibly</p>
                    </div>
                    <div className='Airbnb'>
                        <h4>Newsroom</h4>
                        <p>Learn about new features</p>
                        <p>Letter from our founders</p>
                        <p>Careers</p>
                        <p>Investors</p>

                    </div>
                </div>
            </div>


        </>
    )
}

export default FooterPerPage
