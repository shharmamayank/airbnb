import React from 'react'

export default function Footer() {
  return (
    <div>
      <footer class="footer mt-auto py-3 bg-body-tertiary border border-secondary">
        <div class=" d-flex justify-content-between main-page-footer-main-container">
          <div className='d-flex'>
            <p>© 2023 Airbnb, Inc.</p>
            <p>Privacy</p>
            <p>&nbsp;Terms</p>
            <p>&nbsp;Sitemap</p>
            <p>&nbsp;Company details·</p>
            <p>&nbsp;Destinations</p>
          </div>
          <div className='d-flex justify-content-evenly'>
            <p><i class="fa-sharp fa-solid fa-globe"> </i>&nbsp;English (IN)</p>
            <p> &nbsp;₹ INR</p>
            <p> &nbsp;Support & resources</p>

          </div>
        </div>
      </footer>
    </div>
  )
}
