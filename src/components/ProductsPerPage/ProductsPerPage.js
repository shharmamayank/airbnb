import React from 'react'
import "./ProductsPerPage.css"
import logo from "../../assests/long-logo.png";
import { useDispatch, useSelector } from 'react-redux'
import Calendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import { Link } from 'react-router-dom';
function ProductsPerPage() {
  const { rooms } = useSelector(store => store.categoreyBar)
  console.log(rooms[0].images)
  return (
    <>
      <div className='navbar'>
        <Link to="/" ><img src={logo} alt="" className='navbar-img' />
        </Link>
        <div className='search-bar-2'>
          <div className='search-bar-text3-second'>Start your Search</div>
          <div className='search-bar-icon-div'><i class="fa-solid fa-magnifying-glass"></i></div>
        </div>
        <div className='profile-container'>
          <div className='airbnb-your-home'>Airbnb your home</div>
          <div className='airbnb-your-home'> <i class="fa-sharp fa-solid fa-globe"></i></div>
          <div class="btn-group dropstart">
            <button class="btn btn-secondary text-secondary bg-light rounded-pill" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              <i class="fa-solid fa-bars"></i>
              <i class="fa-solid fa-circle-user"></i>
            </button>
            <ul class="dropdown-menu ">
              <li> <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <a class="dropdown-item" href="#"><b>Sign Up</b></a></button></li>
              <li><a class="dropdown-item" href="#">Login</a></li>
              <div style={{ height: "1px", backgroundColor: "var(--grey)", width: "100%" }}></div>
              <li><a class="dropdown-item" href="#">Airbnb your home</a></li>
              <li><a class="dropdown-item" href="#">Help</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div className='check'>
        <div>
          <h3>{rooms[0].name} </h3>
          <i class="fa-solid fa-star"></i>
          <span> &nbsp;{rooms[0].number_of_reviews.$numberInt} &nbsp;review</span>
          <span>{rooms[0].address.street}</span>
        </div>
        <div className='share-container'>
          <span>
            <i class="fa-solid fa-arrow-up-from-bracket"></i>
            <span>&nbsp; share &nbsp;</span>
          </span>
          <span>
            <i class="fa-solid fa-heart"></i>
            <span>&nbsp; save</span>
          </span>
        </div>
      </div>
      <div className='productperpage-image-conatiner'>
        <div>
          <img className='productperpage-image-big rounded-start' src={rooms[0].images.picture_url} alt="" />
        </div>
        <div className='productperpage-image-small-contanier'>
          <div className='productperpage-image-small-contanier-1'>
            <img className='productperpage-image-smaall' src={rooms[0].images.picture_url1} alt="" />
            <img className='productperpage-image-smaall' src={rooms[0].images.picture_url2} alt="" />
          </div>
          <div className='productperpage-image-small-contanier-2'>
            <img className='productperpage-image-smaall rounded-end' src={rooms[0].images.picture_url3} alt="" />
            <img className='productperpage-image-smaall rounded-end' src={rooms[0].images.picture_url4} alt="" />
          </div>
        </div>
      </div>
      <div className='summary-and-description'>
        <div className='summary-and-description-second-container'>
          <div className='owner-and-checkoutbox'>
            <div>
              <h3>{rooms[0].summary}</h3>
              <p>{rooms[0].roomDescription}</p>
              <hr></hr>
              <span><i class="fa-solid fa-door-closed"></i>&nbsp;<b>{rooms[0].workspace.aboutWorkspace}</b></span>
              <p>{rooms[0].workspace.WorkSpaceDescription}</p>
              <span><i class="fa-solid fa-key"></i>&nbsp;<b>{rooms[0].checkIN.aboutCheckIN}</b></span>
              <p>{rooms[0].checkIN.CheckInDescription}</p>
              <span><i class="fa-solid fa-calendar"></i>&nbsp;<b>{rooms[0].cancellation_policy}</b></span>
              <hr></hr>
            </div>


          </div>
          <div className='banner-container'>
            <span> <span className='Air-text'><b>air</b></span><span className='cover-text'><b>cover</b></span></span>
            <span>Every booking includes free protection from Host cancellations, listing inaccuracies, and other issues like trouble checking in.
            </span>
            <a>Learn more</a>
            <p>{rooms[0].brief}</p>
            <hr></hr>
          </div>
          <div>
            <h3>Where you'll sleep</h3>
            <img className='bedroom-image rounded' src={rooms[0].bedroom.img} alt="" />
            <h3>Bedroom</h3>
            <p>{rooms[0].bedroom.bedSize}</p>
            <hr></hr>
          </div>
          <div className='amenities-container'>
            <h3>What this place offers</h3>
            <div className='amenitites'>
              <div className='amenities-first-block'>
                {rooms[0].amenities.slice(0, 6).map((data, index) => {
                  return <p>{data}</p>
                })}
              </div>
              <div>
                {rooms[0].amenities.slice(7, 13).map((data) => {
                  return <p>{data}</p>
                })}
              </div>


            </div>
            <div>
              <button type="button" class="btn border border-secondary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Show all {rooms[0].amenities.length} amenities
              </button>
              <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h1 class="modal-title fs-5" id="exampleModalLabel"> What this place offers</h1>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      {rooms[0].amenities.map((data) => {
                        return <p>{data}</p>
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr></hr>
          </div>
          <div>
            <h3 className=''>
              {rooms[0].interaction}
            </h3>
            <div className="calendar-container">
              <div>
                <Calendar showDoubleView className={"check-1"} />
              </div>
            </div>
            <hr></hr>
          </div>
          <div>
            <div>
              <h3>
                4.93 · 14 reviews</h3>
            </div>
            <div className='review-marking'>
              <div className=''>
                <div>
                  <h4> Cleanliness {rooms[0].review_scores.review_scores_cleanliness.$numberInt}</h4>
                  <h4>Communication {rooms[0].review_scores.review_scores_communication.$numberInt}</h4>
                  <h4>Check-in{rooms[0].review_scores.review_scores_checkin.$numberInt}</h4>
                </div>
                <div>
                  {rooms[0].reviews.map((data, index) => {
                    return <p>{data.comments}</p>
                  })}
                </div>
              </div>
              <div>
                <div>
                  <h4> Accuracy {rooms[0].review_scores.review_scores_accuracy.$numberInt}</h4>
                  <h4>Location{rooms[0].review_scores.review_scores_location.$numberInt}</h4>
                  <h4>Value{rooms[0].review_scores.review_scores_value.$numberInt}</h4>
                </div>
                <div>
                  {rooms[0].reviews.map((data, index) => {
                    return <p>{data.comments}</p>
                  })}
                </div>
              </div>
            </div>
            <hr></hr>
          </div>
          <div>
            <h3>Where you’ll be</h3>
            <iframe width="100%" height="100%"
              id="gmap_canvas" src="https://maps.google.com/maps?q=kodaikanal&t=&z=10&ie=UTF8&iwloc=&output=embed"
              frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
          </div>
        </div>

        <div className='checkout-box'>
          <h3>₹1,500 night</h3>
          <div className='checkin-and-checkout'>
            <div className='check-in border'>
              <h4>checkIN</h4>
            </div>
            <div className='check-out border'>
              <h4>checkOut</h4>
            </div>
          </div>
          <div class="dropdown">
            <button class="btn dropdown-toggle w-100 d-flex justify-content-between  align-items-center" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              guests
            </button>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="#">Adult</a></li>
              <li><a class="dropdown-item" href="#">Children </a></li>
              <li><a class="dropdown-item" href="#">Infants</a></li>
            </ul>
          </div>
          {/* <Link to="checkout" > */}
            <button type="button" class="btn btn-danger btn-lg btn-block w-100 ">Reserve</button>
            {/* </Link> */}
          <div className='d-flex justify-content-center'>
            <p>You won't be charged yet</p>
          </div>
          <div className='d-flex justify-content-between'>
            <p>₹1,500 x 10 nights </p>
            <p>₹15,000</p>
          </div>
          <div className='d-flex justify-content-between'>
            <p>Airbnb service fee </p>
            <p>₹2,118</p>
          </div>
          <div className='d-flex justify-content-between'>
            <p>Total before taxes </p>
            <p>₹19,172</p>
          </div>
        </div>
      </div>

    </>
  )
}

export default ProductsPerPage
