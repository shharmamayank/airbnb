import React, { useState } from 'react'
import validator from "validator";

function Login() {
    const [formData, setFormData] = useState({ phoneNumber: "", });
    const [formLabel, setFormLabel] = useState({
        phoneNumber: "Enter phone number in above box"
    })
    const [isFormSubmit, setIsFormSubmit] = useState(false);
    const handleValueChange = (event) => {
        if (validator.isNumeric(event.target.value) || event.target.value === "") {
            setFormData({ phoneNumber: event.target.value })
        }
    }
    const formOnSubmit = (event) => {
        if (validator.isNumeric(formData.phoneNumber.length !== 10) || formData.phoneNumber === "") {
            setFormLabel({
                phoneNumber: <p>Enter phone Number</p>
            })
        } else if (!validator.isNumeric(formData.phoneNumber)) {
            setFormLabel({ phoneNumber: <p>Invalid phone number</p> })
        } else {
            setIsFormSubmit(true)
        }

    }
    return (
        <>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" >
                    <div class="modal-content" >
                        <div class="modal-header ">
                            <h1 class="modal-title fs-5 " id="text-login">Log In or sign up </h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <h3>Welcome to Airbnb</h3>
                        
                        <div className='phone-container'>
                            <select class="form-select p-3" aria-label="Default select example">
                                <option selected>Country/Region</option>
                                <option value="93AF">Afghanistan (+93)</option>
                                <option value="358AX">Åland Islands (+358)</option>
                                <option value="355AL">Albania (+355)</option>
                                <option value="213DZ">Algeria (+213)</option>
                                <option value="1AS">American Samoa (+1)</option>
                                <option value="376AD">Andorra (+376)</option>
                                <option value="244AO">Angola (+244)</option>
                                <option value="1AI">Anguilla (+1)</option>
                                <option value="1AG">Antigua &amp; Barbuda (+1)</option>
                                <option value="54AR">Argentina (+54)</option>
                                <option value="374AM">Armenia (+374)</option>
                                <option value="297AW">Aruba (+297)</option>
                                <option value="61AU">Australia (+61)</option>
                                <option value="43AT">Austria (+43)</option>
                                <option value="994AZ">Azerbaijan (+994)</option>
                                <option value="1BS">Bahamas (+1)</option>
                                <option value="973BH">Bahrain (+973)</option>
                                <option value="880BD">Bangladesh (+880)</option>
                                <option value="1BB">Barbados (+1)</option>
                                <option value="375BY">Belarus (+375)</option>
                                <option value="45DK">Denmark (+45)</option>
                                <option value="">India (+91)</option>
                            </select>
                            <form onSubmit={formOnSubmit} >
                                <input id="phone-number" maxLength={10} type="tel" class="form-control p-3" value={formData.phoneNumber} onChange={handleValueChange} required aria-describedby="emailHelp" />
                                <label for="phone-number" className="text-secondary ">
                                    {formLabel.phoneNumber}
                                </label>
                                <>{isFormSubmit && (<div><h4>succesfull</h4></div>)}</>
                                <div className='privacy-text'><p>We’ll call or text you to confirm your number. Standard message and data rates apply. Privacy Policy
                                </p>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-danger btn-lg btn-block w-100 " data-bs-dismiss="modal">Continue</button> 
                                </div>
                            </form>
                        </div>
                        </div>

                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable w-100 d-flex justify-content-center" >
                            <div className='all-social-btn w-75 ' >

                                <button type="button" class="btn border btn-lg btn-block w-100 d-flex justify-content-between">
                                    <i className="fa-brands fa-facebook-f" style={{ color: "#155cd5" }}></i>
                                    Continue with facebook
                                </button>

                                <button type="button" class="btn border btn-lg btn-block w-100 d-flex justify-content-between">
                                    <i className="fa-brands fa-google text-danger pe-5 " style={{ color: "#155cd5" }}></i>
                                    Continue with google
                                </button>
                                <button type="button" class="btn border btn-lg btn-block w-100 d-flex justify-content-between">
                                    <i className="fa-brands fa-apple text-secondary" style={{ color: "#155cd5" }}></i>
                                    Continue with apple
                                </button>
                                <button type="button" class="btn btn-Danger border btn-lg btn-block w-100 d-flex justify-content-between">
                                    <i class="fa-solid fa-envelope"></i>
                                    Continue with email
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default Login
